
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}){
  const { name, description, price, _id } = productProp;
  return(
    <div className="cards">

    <Row>
      <Col>
        <Card className="productCard mx-3 my-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button  bg="dark" variant="dark" as={Link} to={`/products/${_id}`}>Details</Button>
            </Card.Body>
          </Card>
      </Col>
    </Row>

    </div>

  )
}