import { Fragment } from 'react';
import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';

export default function Home() {
	
	const data = {
		title: "Akkaw Shop",
		content: "Online Store for Aurora's Local Products",
		destination: "/products",
		label: "Buy Now!"
	}
	return (
		<Fragment>
			<Banner data={data}/>
			{/* <Highlights/> */}
		</Fragment>
	)
};