import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Verify() {
	const { user, setUser } = useContext(UserContext);
	

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [otp, setOtp] = useState("");
	const [email, setEmail] = useState("");




	// State to determine whether the submit button is enabled or disabled
	// Conditional rendering
	// const [isActive, setIsActive] = useState(false);

	function registerUser (e) {

		e.preventDefault();

		fetch(`http://localhost:5000/users/verify`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				otp: otp,
        email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data.email === email && data.otp === otp) {
				
				Swal.fire({
					title: "Verification Successful",
					icon: "success",
					text: "Check your OTP"
				})
			} else {

				Swal.fire({
					title: "Verification Failed",
					icon: "error",
					text: "Please Check your OTP."
				})
			}
		})
	}

	
	

	return(

		(user.id !== null) ?
			<Navigate to="/products"/>

			:

			<Form onSubmit={(e) => registerUser(e)} >

				  <h1 className="text-center my-3">Verification</h1>


          <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value={email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter email" />
			        {/* <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text> */}
			      </Form.Group>

				  <Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Input One Time Pin</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={otp}
				    	onChange={(e) => {setOtp(e.target.value)}}
				    	placeholder="Enter the otp sent in your email" 
				    	required
				    	/>
				  </Form.Group>

			      			<Button  bg="dark" variant="dark" type="submit" id="submitBtn">
			      		 	 Submit
			      			</Button>
			      				     
			    </Form>	
	)
}
