import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
// components
import AddProduct from './pages/AddProduct';
import AppNavbar from './components/AppNavbar';
import AdminDashboard from './pages/AdminDashboard';
import Products from './pages/Products';
import ProductView from './pages/ProductView'
import EditProduct from './pages/EditProduct';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Verify from './pages/Verify'
import Orders from './pages/Orders';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
  
  useEffect(() => {
      fetch(`http://localhost:5000/users/details`, {
        headers: { 
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {

          // user is logged in
          if(typeof data._id !== "undefined") {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
          } else { 
            setUser({
              id: null,
              isAdmin: null
            })

          }
      })

  }, []);


  return (

    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar/>
            <Container>
              <Routes>
                <Route path="/admin" element={<AdminDashboard/>} />
                <Route path="/addProduct" element={<AddProduct/>} />
                <Route path="/editProduct/:productId" element={<EditProduct/>} />
                <Route path="/products" element={<Products/>} />
                <Route path="/products/:productId" element={<ProductView/>} />
                <Route path="/" element={<Home/>} />
                <Route path="/login" element={<Login/>} />
                <Route path="/logout" element={<Logout/>} />
                <Route path="/register" element={<Register/>} />
                <Route path="/verify" element={<Verify/>} />
                <Route path="/orders" element={<Orders/>} />


                <Route path="*" element={<Error/>} />
              </Routes>
            </Container>
        </Router>
    </UserProvider>
    
  );
}

export default App;
